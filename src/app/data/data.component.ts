import { Component, OnInit } from '@angular/core';
import { RecordsService } from '../records.service'

interface myData{
  geonames: Object
}


@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  constructor(private recordService : RecordsService) { }

  continents = []

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.recordService.getData<myDate>().subscribe(data => {
      this.continents = data.geonames
    })
  }

}
