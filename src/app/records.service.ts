import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class RecordsService {

  constructor(private http: HttpClient) {

   }

  getData(){
    return this.http.get('http://www.geonames.org/childrenJSON?geonameId=6295630')
  }
}
